/* SQL CAPSTONE */

/* Create an activity.txt inside s05 project and create the SQL code for the following: */

-- Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers
	WHERE country = 'Philippines';


-- Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers
	WHERE customerName = 'La Rochelle Gifts';


-- Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products
	WHERE productName = 'The Titanic';


-- Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees
	WHERE email = 'jfirrelli@classicmodelcars.com';


-- Return the names of customers who have no registered state
SELECT customerName FROM customers
	WHERE state IS NULL;


-- Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT  firstName, lastName, email FROM employees
	WHERE lastName = 'Patterson'
		AND firstName = 'Steve';


-- Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers
	WHERE country != 'USA' AND creditLimit > 3000;


-- Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders
	WHERE comments LIKE '%DHL%';


-- Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productlines
	WHERE textDescription LIKE '%state of the art%';


-- Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;


-- Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;


-- Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers
	WHERE country = 'USA' OR country = 'FRANCE' OR country = 'Canada';


-- Return the first name, last name, and office's city of employees whose offices are in Tokyo

-- For Tokyo, GET officeCode
SELECT officeCode, city FROM offices
	WHERE city = 'Tokyo'; -- ANSWER: officeCode = 5


SELECT firstName, lastName, officeCode FROM employees
	WHERE officeCode = 5;


-- Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT customerName, firstName, lastName FROM customers
	RIGHT JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE firstName = 'Leslie' AND lastName = 'Thompson';


-- Return the product names and customer name of products ordered by "Baane Mini Imports"

/* SELECT * FROM table1
	JOIN table2 ON table1.primarykey = table2.foreignkey
	JOIN table3 ON table2.primarykey = table3.foreignkey */

SELECT productName, customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customerName = 'Baane Mini Imports';


-- Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are located in the same country

/* SELECT * FROM table1
	JOIN table2 ON table1.primarykey = table2.foreignkey
	JOIN table3 ON table2.primarykey = table3.foreignkey */

SELECT firstName, lastName, customerName, offices.country FROM offices
	JOIN employees ON offices.officeCode = employees.officeCode
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
	WHERE offices.country = customers.country;


-- Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
SELECT productName, quantityInStock FROM products
	WHERE productLine = 'Planes' AND quantityInStock < 1000;


-- Return the customer's name with a phone number containing "+81".
SELECT customerName FROM customers
	WHERE phone LIKE '%+81%';